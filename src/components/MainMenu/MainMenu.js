import React from 'react';
import {Container, Nav, Navbar} from 'react-bootstrap';




export class MainMenuItem{
  text= '';
  link= '#';

  constructor(text,link){
    this.text=text;
    this.link=link;
  }
}

export class MainMenu extends React.Component{
  
  
  constructor(props){
    super(props);
   
    this.state ={
   
      items:props.items,

    };

   

   
    
  }
  
    render() {
        return (
            <Container>
            <Navbar bg="dark" className='mb-2'  variant="dark">
    
      {this.state.items.map(item=>{
        return(
          <Nav.Link className='bg-warning' href={item.link}>{item.text}</Nav.Link>
        );

      })
     
    }


 

  </Navbar>
  

  </Container>
        );
    }
}