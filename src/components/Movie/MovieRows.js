import React from 'react';
import { Container } from 'react-bootstrap';
import 'jquery/dist/jquery.js';
import 'popper.js/dist/popper.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import axios from "axios";





class MovieRows extends React.Component {
  state={
      movie:{}
  }
 
       
    constructor(props){
        super(props);

            axios
                .get(
                    `https://www.omdbapi.com/?apikey=61771398&i=${
                        this.props.movieID}&plot=full`
                )
                .then(res => res.data)
                .then(res => {
                    this.setState({ movie: res });
                });
        
     
        
        
    }
   
      
   
    render(){
        
        const {
            Title,
            Released,
            Genre,
            Plot,
            Poster,
            imdbRating
        } = this.state.movie;
        if (!Poster || Poster === 'N/A') {
            return null;
        }
        return (
            
            <Container>
    <table>
        <tr>
            <td>
                <img width='200px' src={Poster}/>
            </td>
            <td width='250px'>
            <h4> {Title}</h4>
        <p>{Plot}</p>
            </td>
        </tr>
    </table>



   
    
       
        </Container>
        );
    }



   
}


export default MovieRows;
