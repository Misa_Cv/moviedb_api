import React from 'react';
import axios from "axios";
import  'bootstrap/dist/css/bootstrap.min.css';
import { Container,Card,Button } from 'react-bootstrap';



class MovieCard extends React.Component {


state = {
    movieData: {}
};

componentDidMount() {
    axios
        .get(
            `https://www.omdbapi.com/?apikey=61771398&i=${
                this.props.movieID
            }`
        )
        .then(res => res.data)
        .then(res => {
            this.setState({ movieData: res });
            
            
        });
       
}

render() {
    const {
        Title,
        //Released,
        //Genre,
        Plot,
        Poster,
       // imdbRating
    } = this.state.movieData;

    if (!Poster || Poster === 'N/A') {
        return null;
    }
   
return (
   <Container>
<div className='bg-dark'>

<table className='ml-4 mb-4' style={{float:'left',width:'390px',height:'240px'}} >
    <tr style={{border:'1px solid' ,float:'left'}} >
        <td>
            <img style={{width:'200px'}} src={Poster} alt='poster'/>
        </td>
<td className='bg-dark' style={{width:'250px',fontWeight:'bold',paddinTop:'0',fontSize:'20px',textAlign:'center',color:'white'}}>{Title}

<p style={{width:'250px',padding:'0 10px',fontWeight:'normal',textAlign:'justify',fontSize:'15px'}}>{Plot}</p>

<button className='btn btn-primary mt-6'>Info o filmu</button>
</td>




    </tr>





</table>
</div>




</Container>
);

}

}

export default MovieCard;